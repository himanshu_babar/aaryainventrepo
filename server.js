const express = require('express')
const app = express()
const db = require('./dbconfig/databaseConnection')
const router = require('./Routes/userroute')
const users = require('./Controller/getusers')
app.use(express.json()) // json
app.use('/api', router) // /api 
app.listen(3000, () => {
    console.log("app running ");
})
