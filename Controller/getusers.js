const db = require('../dbconfig/databaseConnection')
const bcrypt = require('bcrypt');
console.log("---------------db", db);
getusers = (req, res) => {
    const sql = 'select user_name , user_email ,user_role,company_id from user';
    db.query(sql, (err, data) => {
        if (!err) {
            res.status(200)
            res.send(data)
        }
        else {
            res.status(500)
            console.log(err);
        }
    })

}
addUsers = (req, res) => { 
    try {
        const id = req.body.user_id;
        const name = req.body.user_name;
        const email = req.body.user_email;
        const password = req.body.user_password;
        const role = req.body.user_role;
        const from = req.body.user_effective_from;
        const till = req.body.user_effective_till;
        const company = req.body.company_id;
        let isValidPassword = password_convert(password);
        var sql = 'INSERT INTO user(user_id,user_name, user_email, user_password ,user_role,user_effective_from,user_effective_till,company_id)VALUES(?,?,?,?,?,?,?,?)';
        db.query(sql, [id, name, email, isValidPassword, role, from, till, company], (err, result) => {
            if (err) {
                res.send(err)
            } else {
                res.send('User Data Inserted Successfully')
            }
        })
    }
    catch (err) {
        res.status(500).json(" User Data Not Inserted ! ")
    }

}
searchuser = (req, res) => {
    db.query('select * from user where user_id = ? ', [req.params.id], (err, data) => {
        if (!err) {
            if (Object.entries(data).length === 0) {
                res.status(500).json("User Invalid");
            } else {
                res.send(data);
            }
        }
    })
}


function password_convert(password) {
    const encryptedPassword = bcrypt.hashSync(password, 10);
    return encryptedPassword;

}


module.exports = { getusers, addUsers, searchuser};




