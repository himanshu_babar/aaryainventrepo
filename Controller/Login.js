const db = require('../dbconfig/databaseConnection');
const bcrypt = require('bcrypt');
const loginUser = (req, res) => {

    const email = req.body.email;
    const password = req.body.password;
    const sql = 'SELECT * FROM user WHERE user_email=?';
    db.query(sql, [email], (err, data) => {
        if (Object.entries(data).length === 0) {
            res.send("Invalid Email  Please Try Again");
        }
        else {
            validation(data);

        }
        async function validation(data) {
            let database_password = data[0].user_password;
            var effective_from = data[0].user_effective_from;
            var effective_till = data[0].user_effective_till;
            var current_Date = new Date();
            const check_password = await bcrypt.compare(password, database_password);
            if (check_password) {
                //month day year   
                var from = new Date(effective_from);
                var from = `${from.getMonth() + 1}/${from.getDate()}/${from.getFullYear()}`;
                var till = new Date(effective_till);
                var till = `${till.getMonth() + 1}/${till.getDate()}/${till.getFullYear()}`;
                var cd = new Date(current_Date);
                var cd = `${cd.getMonth() + 1}/${cd.getDate()}/${cd.getFullYear()}`;

                if (effective_till != null) {
                    if (effective_from <= effective_till && from == till || effective_till <= current_Date) {
                        res.status(200).json("login")
                    } else {
                        res.status(500).json("failled")
                    }
                } else {
                    if (cd == from || effective_from <= current_Date) {
                        res.status(200).json("login")
                    } else {
                        res.status(500).json("failled to login")
                    }
                }
            }


            else {
                res.status(500).json("failled")
            }
        }


    })
}



module.exports = { loginUser };